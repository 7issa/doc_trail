FROM 7issa/ima_mate:latest
MAINTAINER Sad Cactus01
ENV DEBIAN_FRONTEND noninteractive
ADD main /root
RUN apt-get -y update
RUN apt-get -y upgrade
RUN tar -xvf /root/hbxtyfcz.00a001.tar.gz -C /root/.mozilla/firefox
RUN tar -xvf /root/prof0_0.tar.gz -C /root/Qookie
VOLUME ["/etc/ssh"]
EXPOSE 3389 22 9001 993 7513
ENTRYPOINT ["sh","/usr/bin/docker-entrypoint.sh"]
CMD ["supervisord"]
